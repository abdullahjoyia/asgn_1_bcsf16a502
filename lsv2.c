#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include<sys/stat.h>
#include <stdlib.h>
#include <errno.h>

extern int errno;
void do_ls(char*);
int main(int argc, char* argv[])
{
      int i = 0;
      while(++i < argc){
         printf("Directory listing of %s:\n", argv[i] );
	 do_ls(argv[i]);
      }
   }
   
   return 0;
}

int filterHiddenDir(const struct dirent * dir)
{
	if(dir->d_name[0]=='.')
		return 0;
	return 1;
}

int alphaCmp(void * a, void * b)
{
	return strcasecmp((*(struct dirent **)a)->d_name, (*(struct dirent **)b)->d_name);
}

void do_ls(char * dir)
{
   struct dirent ** entryList;
   DIR * dp = opendir(dir);
   if (dp == NULL){
      fprintf(stderr, "Cannot open directory:%s\n",dir);
      return;
   }
   int n = scandir(dir, &entryList, &filterHiddenDir, alphaCmp);
   int i = 0;
   while(i < n)
   {
	printf("%s\n", entryList[i]->d_name);
	free(entryList[i]);
	i++;
   }
   free(entryList);
   closedir(dp);
}