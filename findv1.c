#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#define PATH_MAX 4096
extern int errno;

int isDirectory(const char *path) {
   struct stat statbuf;
   if (stat(path, &statbuf) != 0)
       return 0;
   return S_ISDIR(statbuf.st_mode);
}

int find(char drc[], char fname[]){
	DIR * dp = opendir(drc);
	chdir(drc);
	errno = 0;
	struct dirent* entry;
	while(1) {
		entry = readdir(dp);
		//printf("%s\n", entry->d_name);
		if(entry == NULL && errno != 0){
		    //perror("readdir");
		    return errno;
	        }
	        if(entry == NULL && errno == 0){
		    //printf("\nEnd of directory %s\n", drc);
		    return 0;
	        }
		if(entry->d_name[0] == '.'){
		    continue;
		}
		char newpath[4096] = "";
		strcat(newpath, drc);
		strcat(newpath, "/");
		strcat(newpath, entry->d_name);
		if(strcmp(entry->d_name, fname) == 0){
			printf("%s \n", newpath);
		}
		if(isDirectory(newpath) == 1) {
			find(newpath, fname);
		}
	}
	closedir(dp);
}

int main(int argc, char * argv[]){
	char cwd[PATH_MAX];
	getcwd(cwd, sizeof(cwd));
	if (argc == 3){
		find(argv[1], argv[2]);
	}
	else{
		find(cwd, argv[1]);
	}