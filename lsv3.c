#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include<sys/stat.h>
#include <stdlib.h>
#include <errno.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>

extern int errno;

char* getFileMode(char *file)
{
    struct stat st;
    char *modevalue = malloc(sizeof(char) * 10 + 1);
    if(stat(file, &st) == 0)
	{
        mode_t perm = st.st_mode;
	if ((st.st_mode &  0170000) == 0010000) 
		modevalue[0] = 'p';
	else if ((st.st_mode &  0170000) == 0020000) 
		modevalue[0] = 'c';
	else if ((st.st_mode &  0170000) == 0040000) 
		modevalue[0] = 'd';
   	else if ((st.st_mode &  0170000) == 0060000) 
		modevalue[0] = 'b';
	else if ((st.st_mode &  0170000) == 0100000) 
		modevalue[0] = 'f';
	else if ((st.st_mode &  0170000) == 0120000) 
		modevalue[0] = 'l';
   	else if ((st.st_mode &  0170000) == 0140000) 
		modevalue[0] = 's';
   	else 
		printf("Mode not recoqnized\n");
        modevalue[1] = (perm & S_IRUSR) ? 'r' : '-';
        modevalue[2] = (perm & S_IWUSR) ? 'w' : '-';
        modevalue[3] = (perm & S_IXUSR) ? 'x' : '-';
        modevalue[4] = (perm & S_IRGRP) ? 'r' : '-';
        modevalue[5] = (perm & S_IWGRP) ? 'w' : '-';
        modevalue[6] = (perm & S_IXGRP) ? 'x' : '-';
        modevalue[7] = (perm & S_IROTH) ? 'r' : '-';
        modevalue[8] = (perm & S_IWOTH) ? 'w' : '-';
        modevalue[9] = (perm & S_IXOTH) ? 'x' : '-';
        modevalue[10] = '\0';
        return modevalue;     
    }
    else
	{
        return strerror(errno);
    }   
}

int filterHiddenDir(const struct dirent * dir){
	if(dir->d_name[0]=='.')
		return 0;
	return 1;
}

int alphaCmp(void * a, void * b)
{
	return strcasecmp((*(struct dirent **)a)->d_name, (*(struct dirent **)b)->d_name);
}